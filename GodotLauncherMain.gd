const Utils = preload('./Utils.gd')

export(String, FILE, "*.tscn") var main_scene = null
export(String, FILE, "*.tscn") var launcher_scene = null
export(String, FILE, "*.tscn") var launcher_background = null

export(int, 60, 1024) var launcher_width = 600
export(int, 60, 1024) var launcher_height = 600
export(bool) var launcher_borderless = true
#export(bool) var spawn_new_process = true

# For now, just always spawn new process, since its most reliable
const SPAWN_NEW_PROCESS = true

var launcher
var launcher_bg
var use_launcher = false

func launcher():
    var dimensions = Vector2(launcher_width, launcher_height)

    # Stash away original width and height
    var orig_width = Globals.get('display/width')
    var orig_height = Globals.get('display/height')
    Globals.set('launcher_original/display_width', orig_width)
    Globals.set('launcher_original/display_height', orig_height)

    # First ensure not fullscreen
    OS.set_window_fullscreen(false)

    # Then resize and hide border if applicable
    OS.set_borderless_window(launcher_borderless)
    OS.set_window_resizable(true)
    OS.set_window_size(dimensions)

    var center = OS.get_screen_size() / 2
    OS.set_window_position(center - (dimensions / 2))

    # For some reason we need to delay this
    call_deferred("_set_launcher_unresizable")

    if launcher_background:
        var LauncherBackground = load(launcher_background)
        launcher_bg = LauncherBackground.instance()
        add_child(launcher_bg)

    # Add actual launcher menu
    var launcher_scene_path = "./Launcher.tscn"
    if launcher_scene:
        launcher_scene_path = launcher_scene
    var Launcher = load(launcher_scene_path)
    launcher = Launcher.instance()
    # TODO make deferred call, so background renders first?
    add_child(launcher)

func _set_launcher_unresizable():
    OS.set_window_resizable(false)

func game():
    Utils.save() # Ensure the settings are saved
    if SPAWN_NEW_PROCESS:
        Utils.launch_game()
        get_tree().quit() # Quit main area
    else:
        # Loads new settings from file (only works for some)
        Utils.force_settings_to_take_effect()
        # load new screen
        directly_to_game()

func directly_to_game():
    # Just switch to the main scene of the game
    get_tree().change_scene(main_scene)

func _ready():
    # Graphical desktop, use launcher
    var platform = OS.get_name()
    if platform == "OSX" or platform == "Windows" or platform == "X11":
        use_launcher = true

    var args = Array(OS.get_cmdline_args())
    if args.has('--skip-launcher'):
        use_launcher = false

    if use_launcher:
        launcher()
    else:
        directly_to_game()
