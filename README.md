UNFINISHED, WIP
---------------

(I wrote documentation first, this is very much in progress.)

Godot Launcher
-----------------

Godot Launcher is full featured set of helper scenes for creating video
games with user-friendly launchers and customizable controls.

Originally made for the [puzzle game Sawdust](http://michaelb.org/sawdust/).


Usage
-----

1. Download this library, either checkout via git (perhaps a as a
submodule) or simply download the zip file

2. Create a scene file that extends `GodotLauncherMain` and set this as
your project's main file

3. Set "Main Scene" to the actual main scene for your game.

Now, when your game is started without arguments, it will go to the
launcher shown above. This is often enough for most games: it offers
controller re-assignment, video options, custom resolutions, etc.


Options
-------

* Set "Launcher Background Scene" to point to a scene
    that will be instanced for the background of the launcher
* Disable window borders for the launcher
* Specify which controller options should be customizable


Advanced
--------

By setting a custom "Launcher Scene", you can further customize the
launcher. This package provides several instanceable scene files that
might be useful for this:

* `ControllerMenu` provides the UI for mapping controller options

* `ControllerMapperOption` provides the UI for mapping a single
  controller option (used by above)

* `VideoModeMenu` provides a dropdown of popular resolution
  choices, along with "fullscreen" option

* `GraphicsOptionsMenu` exposes some Godot Engine video options,

* `SoundOptionsMenu` provides volume sliders for music (sets Stream volume
  adjustment) and FX (sets player volume adjustment option)

* `Utils.gd` has a bunch of high-level functions, notably ones for
  starting the game with given options

You can arrange these parts for a custom launcher, and/or include them
in a dedicated settings menu within the game.

Known issues
---------------

* `display/stretch_mode: "2d"` breaks the launcher. I'm not sure anyway around
  this, and I can't figure out a way to force reset it when setting up the
  launcher (suggestions welcome!).


TODO
----

There are a lot of features I want to add:

* Auto-update system
* Scripts for packaging for different platforms (much like what
  electron-builder does for electron apps)
* Better graphics options, and include presets of "High", "Medium", and "Low"
  graphics settings
* Auto-detection of resolution
* Multiple player support, and provide a wrapper around Input that
  abstracts the concept of "player", interchangeable between keyboard /
  controller.

Pull requests are welcome!

