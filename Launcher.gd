onready var application_name = Globals.get('application/name')

func _ready():
    get_node('GameTitle').set_text(application_name)

func _on_PlayButton_pressed():
    get_node('..').game()
