const ControllerAssignment = preload('./ControllerAssignment.tscn')

export(String) var input_name = 'ui_up'

onready var text_label = get_node('Label')
onready var assignments_container = get_node('AssignmentsContainer')
onready var new_assignment_button = get_node('AssignmentsContainer/NewAssignment')

func _ready():
    refresh_list()

func input_name_to_label(name):
    # Capitalize and replaces common acronym
    return name.capitalize().replace('Ui', 'UI')

func refresh_list():
    if not text_label:
        return
    var label_text = input_name_to_label(input_name)
    text_label.set_text(label_text)

    # Clear the initial labels
    _clear_assignments()

    # Add labels
    if InputMap.has_action(input_name):
        var events = InputMap.get_action_list(input_name)
        for event in events:
            var assignment = ControllerAssignment.instance()
            assignments_container.add_child(assignment)
            assignment.set_event(input_name, event)
    assignments_container.move_child(new_assignment_button, 0)

func _clear_assignments():
    for child in assignments_container.get_children():
        if not child extends BaseButton:
            child.remove_and_skip()

var is_recording = false
func _on_NewAssignment_pressed():
    start_recording()

func stop_recording():
    refresh_list()
    new_assignment_button.set_text('+ Assign')
    set_process_input(false)
    is_recording = false

func start_recording():
    if is_recording:
        stop_recording()
        return
    is_recording = true
    set_process_input(true)
    new_assignment_button.set_text('(Cancel)')

    var new_label = Label.new()
    new_label.set_text('Press a key or game controller button...')
    assignments_container.add_child(new_label)
    assignments_container.move_child(new_label, 1)

func _input(event):
    if event.type == InputEvent.KEY or event.type == InputEvent.JOYSTICK_BUTTON:
        get_tree().set_input_as_handled()
        stop_recording()
        InputMap.action_add_event(input_name, event)


