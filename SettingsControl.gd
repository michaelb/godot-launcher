#
# SettingsControl.gd is a re-usable script that stores the value of a
# widget into the global settings
#

const Utils = preload('./Utils.gd')

# Key is the settings key
export(String) var key = null

func _ready():
    if key == null:
        print("ERROR: No key specified on control " + get_name())
        return
    # Load the value from settings to update the initial position
    var my_value = Utils.get(key)
    if self extends CheckButton:
        set_pressed(my_value)
        self.connect("toggled", self, "_was_value_changed", [])
    else:
        set_value(my_value)
        self.connect("value_changed", self, "_was_value_changed", [])

func _get_my_value():
    # Gets the value based on what type of control this is
    if self extends CheckButton:
        return self.is_pressed()
    else:
        return self.get_value()

func _was_changed():
    # Something was changed, store the new value
    var value = _get_my_value()
    Utils.change(key, value)

func _was_value_changed(new_value):
    # Same as above, but the value is directly passed in
    Utils.change(key, new_value)
