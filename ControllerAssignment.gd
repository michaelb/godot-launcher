# Uses XBox convention
const CONTROLLER_LABELS = {
    1: 'B',
    0: 'A',
    2: 'X',
    3: 'Y',
    10: 'Select (Back)',
    11: 'Start (Menu)',
    12: 'D-Pad Up',
    13: 'D-Pad Down',
    14: 'D-Pad Left',
    15: 'D-Pad Right',
    4: 'L1 (Trigger)',
    6: 'L2 (Shoulder)',
    8: 'Left stick click',
    5: 'R1 (Trigger)',
    7: 'R2 (Shoulder)',
    9: 'Right stick click',
}

onready var event_description_label = get_node('EventDescriptionLabel')
var event = null
var name = null

func joy_button_string(value):
    if CONTROLLER_LABELS.has(value):
        return CONTROLLER_LABELS[value]
    return str(value)

func describe_input_event(event):
    # Capitalize and replaces common acronym
    var type = event.type
    var prefix = ''
    var suffix = '.'
    if type == InputEvent.KEY:
        # prefix already correct
        suffix = OS.get_scancode_string(event.scancode)
    elif type == InputEvent.JOYSTICK_BUTTON:
        prefix = 'Joy #' + str(event.device) + ' '
        suffix = joy_button_string(event.button_index)
    else:
        # Should skip this event
        return null
    return prefix + suffix

func set_event(name, event):
    self.event = event
    self.name = name
    var label_text = describe_input_event(event)
    if label_text == null:
        return
    event_description_label.set_text(label_text)

func _on_UnassignButton_pressed():
    # Remove from input
    InputMap.action_remove_event(name, event)
    remove_and_skip()
