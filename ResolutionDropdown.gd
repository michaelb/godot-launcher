const Utils = preload('./Utils.gd')

const DISPLAY_WIDTH = 'display/width'
const DISPLAY_HEIGHT = 'display/height'

func _ready():
    # Load the value from settings to update the initial position
    var original_width = Utils.get(DISPLAY_WIDTH)
    var original_height = Utils.get(DISPLAY_HEIGHT)

    # First, loop through resolutions and check if we have one already
    # selected, and make a note of where it appears
    var selected_reso = null
    var selected_idx = 0
    var idx = 0
    for reso in Utils.get_resolutions():
        if reso[0] == original_width and reso[1] == original_height:
            selected_reso = reso
            selected_idx = idx
            break
        idx += 1

    if selected_reso == null:
        # Insert a new selection of current one to top, with separator
        var fake_reso = [original_width, original_height, null, null]
        add_item(Utils.resolution_to_label(fake_reso))
        add_separator()

    for reso in Utils.get_resolutions():
        # Loop through and add all items
        add_item(Utils.resolution_to_label(reso))

    # Finally, set the default selected one to either the top thing, or
    # the one that was previously selected
    select(selected_idx)
    self.connect("item_selected", self, "_was_value_changed", [])

func _was_value_changed(idx):
    # A new value got selected, this gets the label and attempts to set
    # the properties as requested
    var value_array = Utils.resolution_split_label(get_item_text(idx))
    Utils.change(DISPLAY_WIDTH, value_array[0])
    Utils.change(DISPLAY_HEIGHT, value_array[1])
