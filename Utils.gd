const GAME_SETTINGS_PREFIX = 'launcher_game_settings/'

# GRAPHICS settings
const GRAPHICS_SHADOWS = 'render/shadows_enabled'

# VIDEO settings
const VIDEO_FULLSCREEN = 'display/fullscreen'

const POPULAR_RESOLUTIONS = [
    '864x486 (16:9)',
    '800x600 (4:3)',
    '960x540 (16:9)',
    '1024x576 (16:9)',
    '1024x768 (4:3)',
    '1280x720 (16:9)',
    '1152x864 (4:3)',
    '1368x768',
    '1280x960 (4:3)',
    '1440x900 (8:5)',
    '1280x1024 (5:4)',
    '1600x900 (16:9)',
    '1400x1050 (4:3)',
    '1600x1024',
    '1680x1050 (8:5)',
    '1920x1080 (16:9)',
    '2560x1600 (8:5)',
    '2880x1800 (8:5)',
    '3840x2160 (16:9)',
    '4096x2160',
]

const RATIO_16_9 = '16:9'
const RATIO_8_5 = '8:5'
const RATIO_5_4 = '5:4'
const RATIO_4_3 = '4:3'
const NONE = null

const RESOLUTIONS = [
    [864,  486,  16, 9],
    [800,  600,  4, 3] ,
    [960,  540,  16, 9],
    [1024, 576,  16, 9],
    [1024, 768,  4, 3],
    [1280, 720,  16, 9],
    [1152, 864,  4, 3],
    [1368, 768,  null, null],
    [1280, 960,  4, 3],
    [1440, 900,  8, 5],
    [1280, 1024, 5, 4],
    [1600, 900,  16, 9],
    [1400, 1050, 4, 3],
    [1600, 1024, null, null],
    [1680, 1050, 8, 5],
    [1920, 1080, 16, 9],
    [2560, 1600, 8, 5],
    [2880, 1800, 8, 5],
    [3840, 2160, 16, 9],
    [4096, 2160, null, null],
]

static func resolution_split_label(label):
    # Splits a string in the format of
    # '1600x900 (16:9)' into [1600, 900]
    # The ratio is optional, and discarded.
    var split = label.split(' ')
    var reso = split[0]
    var split_reso = reso.split_floats('x')
    return [int(split_reso[0]), int(split_reso[1])]

static func resolution_to_label(resolution_array):
    var prefix = str(resolution_array[0]) + 'x' + str(resolution_array[1])
    if resolution_array[2] == null:
        return prefix
    var suffix = str(resolution_array[2]) + ':' + str(resolution_array[3])
    return prefix + ' (' + suffix + ')'

static func get_resolutions():
    var reso = OS.get_screen_size()
    var width = reso[0]
    var height = reso[1]
    return get_resolutions_smaller_than(width, height)

static func get_resolutions_by_ratio(ratio):
    var filtered_resolutions = []
    var split = ratio.split_floats(':')
    var split_l = int(split[0])
    var split_r = int(split[1])
    for reso in RESOLUTIONS:
        if reso[2] == split_l and reso[3] == split_r:
            filtered_resolutions.append(reso)
    return filtered_resolutions

static func get_resolutions_smaller_than(width, height):
    var filtered_resolutions = []
    for reso in RESOLUTIONS:
        if reso[0] <= width and reso[1] <= height:
            filtered_resolutions.append(reso)
    return filtered_resolutions

static func timeout(node, method_name, seconds):
    #  Simple setTimeout type function. Used as such:
    # timeout(self, "change_scenes", 5)
    var timer = Timer.new()
    node.add_child(timer)
    timer.connect("timeout", node, method_name)
    timer.set_wait_time(seconds)
    timer.set_one_shot(true)
    timer.start()

static func change(key, value):
    Globals.set_persisting(key, true)
    Globals.set(key, value)

static func change_for_game(key, value):
    # Used for custom ones
    var settings_key = GAME_SETTINGS_PREFIX + key
    Globals.set(settings_key, value)
    Globals.set_persisting(settings_key, true)

static func get(key):
    return Globals.get(key)

static func save():
    # TODO: save and load from userdata, so that it work with RO
    # filesystems
    Globals.save()

static func launch_game():
    # Lets now the actual game
    var path = OS.get_executable_path()
    var args = Array(OS.get_cmdline_args())
    args.append('--skip-launcher') # Ensure it starts the game this time
    OS.execute(path, args, false)

static func launch_launcher(width, height):
    # Lets now the actual game
    var path = OS.get_executable_path()
    var args = Array(OS.get_cmdline_args())
    args.append('-w') # Force windowed
    args.append('-r') # Request resolution
    args.append(str(width) + 'x' + str(height)) # Concatenate resolution args
    args.append('--force-launcher') # Ensure it starts the launcher this time
    print("EXCITING", args)
    OS.execute(path, args, false)

static func force_settings_to_take_effect():
    # Check for stuff that we have to set right away

    # Audio
    var value
    value = get('audio/fx_volume_scale')
    AudioServer.set_fx_global_volume_scale(value)
    value = get('audio/stream_volume_scale')
    AudioServer.set_stream_global_volume_scale(value)

    # Video mode
    var fullscreen = get('display/fullscreen')
    OS.set_window_resizable(true)
    OS.set_window_fullscreen(fullscreen)
    if not fullscreen:
        OS.set_borderless_window(false)
        var dimensions = Vector2(get('display/width'), get('display/height'))
        OS.set_window_size(dimensions)

