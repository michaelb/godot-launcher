const ControllerMapperOption = preload('./ControllerMapperOption.tscn')

func _ready():
    refresh_list()

func refresh_list():
    for action_name in InputMap.get_actions():
        var option = ControllerMapperOption.instance()
        option.input_name = action_name
        var sep = HSeparator.new()
        add_child(sep)
        add_child(option)

